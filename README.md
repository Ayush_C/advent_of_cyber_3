# Advent_of_cyber_3

![Advent_Of_Cyber_3](https://tryhackme.com/img/meta/aoc.png)

## This is my repo. of Try Hack Me Advent of Cyber 3 challenges.

- [Day 1-Web Exploitation Save The Gifts](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%201-Web%20Exploitation%20Save%20The%20Gifts)
- [Day 2-Web Exploitation Elf HR Problems](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%202-Web%20Exploitation%20Elf%20HR%20Problems)
- [Day 3-Web Exploitation Christmas Blackout](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%203-Web%20Exploitation%20Christmas%20Blackout)
- [Day 4-Web Exploitation Santa's Running Behind](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%204-Web%20Exploitation%20Santa's%20Running%20Behind)
- [Day 5-Web Exploitation Pesky Elf Forum](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%205-Web%20Exploitation%20Pesky%20Elf%20Forum)
- [Day 6-Web Exploitation Patch Management Is Hard](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%206-Web%20Exploitation%20Patch%20Management%20Is%20Hard)
- [Day 7-Web Exploitation Migration Without Security](https://gitlab.com/Ayush_C/advent_of_cyber_3/-/tree/main/Day%207-Web%20Exploitation%20Migration%20Without%20Security)
